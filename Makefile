EXTENSION = dict_minlen
EXTVERSION = 1.0
PG_CONFIG = pg_config

MODULE_big = dict_minlen
OBJS = dict_minlen.o

DATA = $(wildcard *.sql)

PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
